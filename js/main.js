
$(".btn-modal").fancybox({
    'padding'    : 0,
    'tpl'        : {
        closeBtn : '<a title="Close" class="btn-close" href="javascript:;"></a>'
    }
});

// Sliders

$('.cooperation-slider').slick({
    arrows: false,
    autoplay: false,
    dots: false,
    slidesToShow: 1,
    slidesToScroll: 1
});


$('.nav.prev').click(function(){
    $('.cooperation-slider').slick('slickPrev');
});

$('.nav.next').click(function(){
    $('.cooperation-slider').slick('slickNext');
});


$('.btn-down').click(function(){
    var str=$(this).attr('href');
    $.scrollTo(str, 500);
    return false;
});


$(function () {
    var filterList = {
        init: function () {
            $('.portfolio-list').mixitup({
                targetSelector: '.portfolio-item',
                filterSelector: '.filter',
                effects: ['fade'],
                easing: 'snap'
            });
        }
    };
    filterList.init();
});


$(document).ready(function() {

    // Анимация
    var Android = navigator.userAgent.search(/Android/i);
    var iPhone = navigator.userAgent.search(/iPhone/i);
    var iPad = navigator.userAgent.search(/iPad/i);
    if(Android != -1 || iPhone != -1 || iPad != -1) {

        $('.video-inner').hide();
        console.log('tab');


    } else {

    }
});